﻿using UnityEngine;

namespace TinyBitTurtle
{
    public abstract class CharacterCtrl : MonoBehaviour
    {
        public Character character;
        public Spawner puffFXSpawner;

        protected Animator animator;
       

        void Start()
        {
            // don't collide with enemies
            Physics2D.IgnoreLayerCollision(8, 15);
        }

        public virtual void Init()
        {
            animator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
        }

        public virtual void onMove(Character character)
        {
        }

        public virtual void EmitPuff()
        {
        }
    }
}