﻿using System;
using System.Collections;
using UnityEngine;

namespace TinyBitTurtle
{
    public class Spawner : EZObjectPool
    {
        // delegate callback
        public delegate void SpawnCallback(GameObject newGameObject);

        public string spawnSound;
        public float delay;

        private void spawnAction(SpawnCallback callback, Vector3 pos)
        {
            // spawn and positionned newly object
            GameObject newGameObject;
            TryGetNextObject(transform.position, transform.rotation, out newGameObject);
            newGameObject.transform.localPosition = pos;

            // do callback
            if(callback != null)
                callback(newGameObject);

            // play spawn sound
            if (spawnSound != "")
                AudioCtrl.Instance.playSoundEvent.Invoke(spawnSound);
        }

        public void spawn(SpawnCallback callback, Vector3 pos)
        {
            // wait X seconds before spawning
            if (delay != 0)
            {
                StartCoroutine(delayedSpawn(callback, spawnAction, delay, pos));
            }
            // spawm immediately
            else
            {
                spawnAction(callback, pos);
            }
        }

        private static IEnumerator delayedSpawn(SpawnCallback callback, Action<SpawnCallback, Vector3> spawnAction, float duration, Vector3 pos)
        {
            // wait
            yield return new WaitForSecondsRealtime(duration);

            // spawn
            spawnAction(callback, pos);
        }
    }
}